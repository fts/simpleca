FROM alpine:edge
MAINTAINER julien.leduc@cern.ch

RUN apk add --update-cache socat curl bash openssl lighttpd && \
    rm -fr /var/cache/apk/* /tmp/*
ADD ./server /opt/ca

RUN chmod a+rx /opt/ca/bin/* /opt/ca/cgi/*

# Fix missing lighthttpd group that prevents lighthttpd from starting
# Looks like this was OK, just that the container had to be rebuild from scratch
#RUN addgroup lighttpd

VOLUME ["/var/lib/ca"]

ENTRYPOINT ["/opt/ca/bin/start.sh"]
