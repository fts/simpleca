# A Simple CA Authority

This is a simple CA authority for auto-issue certificates.
This repository was needed to rebuild dockerhub non functionning [simpleca docker image](https://hub.docker.com/r/easeway/simpleca/): **instantiating this docker image resulted in a failing simpleca instance because of the missing lighthttpd system group**.

## run in `docker`

Run as server (CA authority):

```
docker run -d -v local-cert-dir:/var/lib/ca gitlab-registry.cern.ch/fts/simpleca
```

To retrieve CA certificate:

```
wget http://ip/certs/ca.pem
```

From a client to request a certificate

```
docker run --rm -v local-cert-dir:/var/lib/ca gitlab-registry.cern.ch/fts/simpleca server-ip CommonName AltName1=Val1 AltName2=Val2 ...
```

Here, `local-cert-dir` is a local directory contains private keys and certificates.
A sub-folder `keys` is created for private keys and `certs` is created for certificates.

`AltName?` can be `IP.2=AnotherIP`, `DNS.1=dns1`, `DNS.2=dns2`.
Please note, `IP.1` is automatically determined by server when receiving the HTTP request.


## run in `kubernetes`

Launch the server in a pod using the provided example files.

As *root* create `/root/local-cert-dir` directory, where the server will persistently store the generated files:
```
[root@ctadevjulien ~]# mkdir -p /root/local-cert-dir
```

Then register *simpleca* service in the namespace, we simply go for the *default* namespace here:
```
[root@ctadevjulien ~]# kubectl create -f simpleca-svc.yaml 
service "simpleca" created
```

Now create the *simpleca* pod:
```
[root@ctadevjulien ~]# kubectl create -f pod_simpleca.yaml 
pod "simpleca" created
```

We can now contact our namespace *simpleca* server and start to query it:
```
[root@ctadevjulien ~]# nslookup -timeout=1 simpleca.default.svc.cluster.local 10.254.199.254
Server:		10.254.199.254
Address:	10.254.199.254#53

Name:	simpleca.default.svc.cluster.local
Address: 10.254.44.4

[root@ctadevjulien ~]#  wget 10.254.44.4/certs/ca.pem
--2018-03-12 19:09:44--  http://10.254.44.4/certs/ca.pem
Connecting to 10.254.44.4:80... connected.
HTTP request sent, awaiting response... 200 OK
Length: 1090 (1.1K) [application/octet-stream]
Saving to: ‘ca.pem’

100%[====================================================================>] 1,090       --.-K/s   in 0s      

2018-03-12 19:09:44 (123 MB/s) - ‘ca.pem’ saved [1090/1090]

[root@ctadevjulien ~]# openssl x509 -in ca.pem  -text
Certificate:
    Data:
        Version: 3 (0x2)
        Serial Number:
            fd:ae:d8:ec:67:e5:3b:e2
    Signature Algorithm: sha256WithRSAEncryption
        Issuer: CN=SimpleCA
        Validity
            Not Before: Mar  2 13:26:19 2018 GMT
            Not After : Feb 28 13:26:19 2028 GMT
        Subject: CN=SimpleCA
...
```
